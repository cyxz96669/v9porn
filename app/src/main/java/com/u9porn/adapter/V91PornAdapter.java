package com.u9porn.adapter;

import android.net.Uri;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.liulishuo.filedownloader.model.FileDownloadStatus;
import com.u9porn.MyApplication;
import com.u9porn.R;
import com.u9porn.data.DataManager;
import com.u9porn.data.db.entity.V9PornItem;
import com.u9porn.utils.GlideApp;

import java.util.List;

/**
 * @author flymegoc
 * @date 2017/11/14
 */

public class V91PornAdapter extends BaseQuickAdapter<V9PornItem, BaseViewHolder> {

    public V91PornAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, V9PornItem item) {
        String alreadyDownloaded = "(未下载)";
        DataManager dataManager = MyApplication.getInstance().getDataManager();
        V9PornItem v9PornItems = dataManager.findV9PornItemByViewKey(item.getViewKey());
        if(v9PornItems != null && v9PornItems.getStatus() == FileDownloadStatus.completed) {
            alreadyDownloaded = "(已下载)";
            //Log.d("yangyang",""+item.getTitle()+item.getViewKey());
        }

        helper.setText(R.id.tv_91porn_item_title, alreadyDownloaded+item.getTitleWithDuration());
        helper.setText(R.id.tv_91porn_item_info, item.getInfo());
        ImageView simpleDraweeView = helper.getView(R.id.iv_91porn_item_img);
        Uri uri = Uri.parse(item.getImgUrl());
        GlideApp.with(helper.itemView).load(uri).placeholder(R.drawable.placeholder).transition(new DrawableTransitionOptions().crossFade(300)).into(simpleDraweeView);
    }
}
